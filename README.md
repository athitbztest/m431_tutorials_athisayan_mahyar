# Tutorials zu Microsoft Azure

Willkommen zu den Tutorials zu Microsoft Azure! In diesem Repository finden Sie eine Sammlung von Anleitungen und Videos, die Ihnen helfen, Microsoft Azure besser zu verstehen und zu nutzen.

## Tutorials

### 1. Abo-Freischaltung

Hier finden Sie ein Video, das Ihnen zeigt, wie Sie Ihr Azure-Abonnement aktivieren:

![Datei-Abo-Freischaltung](M431_Tutorial_Abo.mp4)

### 2. SQL-Datenbanken

Schauen Sie sich dieses Video an, um mehr über die Verwendung von SQL-Datenbanken in Azure zu erfahren:

![Datei-SQL-Datenbanken](M431_Tutorial_SQL.mp4)

### 3. Virtuelle Computer

Erfahren Sie, wie Sie virtuelle Computer in Microsoft Azure einrichten und verwalten:

![Datei-Virtuelle-Computer](M431_Tutorial_Azure_VM.mp4)

## Bewertung

Bitte nehmen Sie sich einen Moment Zeit, um unser Feedbackformular auszufüllen und Ihre Erfahrungen mit den Tutorials zu teilen:

[Feedbackformular](https://docs.google.com/forms/d/1dQhQIlW_EiBb2fvy_JYwuPB2HHWd8UFMIcGRmxA45M8)

Vielen Dank für Ihre Teilnahme und Ihr wertvolles Feedback!
